package BankCalc;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static BankCalc.Inputs.*;

/**
 * Класс-библиотека
 */
class ForOtherProgrammers {

    static List<Credits> creditsList = new ArrayList<>(); //лист со всеми кредитами

    public static class Credits {
        long id;
        String name;
        long maxSum;
        int maxTerm;
        public double percent;


        public long getMaxSum() {
            return maxSum;
        }

        int getMaxTerm() {
            return maxTerm;
        }

        @Override
        public String toString() {
            return id + ". " + name;
        }

        Credits(long id, String name, long maxSum, int maxTerm, double percent) {
            this.id = id;
            this.name = name;
            this.maxSum = maxSum;
            this.maxTerm = maxTerm;
            this.percent = percent;
        }
    }

    static void addCredit(long id, String name, long maxSum, int maxTerm, double percent) {
        creditsList.add(new Credits(id, name, maxSum, maxTerm, percent));
    }

    static void filter(List<Credits> list) {
        list.stream()                    //фильтрую на возможные для клиента кредиты по введенным данным
                .filter((p) -> p.getMaxSum() >= sum && p.getMaxTerm() >= termCredit)
                .forEach(System.out::println);
    }

}

/**
 * Класс содержит в себе возврат вводимых с клавиатуры данных пользователя
 */
class Inputs {
    static int sum; //Желаемая сумма кредита введенная пользователем с клавиатуры
    static int termCredit;//Желаемая срок кредита введенная пользователем с клавиатуры
    static int typeCredit;//Выбор конкретного кредита введенная пользователем с клавиатуры
    static int changeCredit;//Выбор схемы расчета кредита (Аннуитентный или Диферентный)

    static void getSum() {
        System.out.println("Введите сумму кредита");
        Scanner in = new Scanner(System.in);
        sum = in.nextInt();
    }

    static void getTerm() {
        System.out.println("Введите срок кредита, в месяцах");
        Scanner term = new Scanner(System.in);
        termCredit = term.nextInt();
    }

    static void getTypeCredit() {
        Scanner getTypeCredit = new Scanner(System.in);
        typeCredit = getTypeCredit.nextInt();
    }

    static void getChangeCredit() {
        System.out.println("Выберете способ оплаты кредита:");
        System.out.println("1.Аннуитентный");
        System.out.println("2.Дифференцированный");
        Scanner getChangeCredit = new Scanner(System.in);
        changeCredit = getChangeCredit.nextInt();
    }
}

/**
 * http://www.sberbank.ru/ru/person/credits/money/
 * Калькуляторы на каджый кредит свои, нужно выбрать тот или иной кредит на сайте по ссылке что находится выше
 * Класс вычисления кредита по аннуитентной и дифферентной "схеме"
 */
class ChangeCredit {
    static void changeCredit() { //метод выбора по какой схеме пойдет расчет кредита(Аннуитентный или Диферентный)
        if (changeCredit == 1) {
            AnnuitentСountCredit.annuitentCountCredit();
        } else if (changeCredit == 2) {
            DifferentiatedCountCredit.differentiatedCountCredit();
        }
    }
}

class AnnuitentСountCredit { //
    private static double monthlyPayment; // ежемесячный расчет
    private static double rateMonthlyPayment;
    private static double ratePercent; //процентная ставка
    private static double a;

    static void annuitentCountCredit() {
        ratePercent = AllCredits.currentPercent / 12 / 100;
        a = Math.pow(1 + ratePercent, termCredit);
        rateMonthlyPayment = (ratePercent * a) / (a - 1);
        monthlyPayment = rateMonthlyPayment * sum;
        System.out.printf("Сумма ежемесячного платежа: " + "%.2f", monthlyPayment);
    }

}

class DifferentiatedCountCredit {

    private static double mainPayment; // основной платеж
    private static double accuredPercent;
    private static double remains;
    private static double thispayment; // сумма платежа данного месяца

    static void differentiatedCountCredit() {

        for (int i = 0; i < termCredit; i++) {
            mainPayment = sum / termCredit;
            remains = sum - (mainPayment * i);
            accuredPercent = remains * (AllCredits.currentPercent / 100 / 12);
            thispayment = mainPayment + accuredPercent;
            System.out.printf(i + 1 + " месяц: " + "%.2f", thispayment);
            System.out.println();
        }
    }
}

