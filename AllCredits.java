package BankCalc;

import java.util.ArrayList;
import java.util.List;

import static BankCalc.ForOtherProgrammers.creditsList;
import static BankCalc.Inputs.*;
 /*
      @autor Belonogov Anton
     * @version 1.0
     */
/**
 * Класс где храняться все имеющиеся в банка кредиты
 * http://www.sberbank.ru/ru/person/credits/money/
 * Калькуляторы на каджый кредит свои, нужно выбрать тот или иной кредит на сайте по ссылке что находится выше
 */
public class AllCredits {
    private static int anyone = 1000000000; //число-бесконечность, для кредитов где нет ограничения по мксимальной сумме
    static double currentPercent = 0.0;
    public static void main(String[] args) {
        Inputs.getSum(); //ввод желаемой суммы кредита пользователя через консоль
        Inputs.getTerm();//ввод желаемой срока кредита пользователя через консоль

        ForOtherProgrammers.addCredit(1, "Потребительский кредит без обеспечения", 1_500_000, 60, 14.9);
        ForOtherProgrammers.addCredit(2, "Потребительский кредит под поручительство физических лиц", 3_000_000, 60, 13.9);
        ForOtherProgrammers.addCredit(3, "Кредит физическим лицам, ведущим личное подсобное хозяйство", anyone, 60, 20);
        ForOtherProgrammers.addCredit(4, "Потребительский кредит военнослужащим - участникам НИС", anyone, 60, 15);
        ForOtherProgrammers.addCredit(5, "Нецелевой кредит под залог недвижимости", anyone, 240, 14);
        System.out.println("Вам доступны следующие кредиты: ");
        ForOtherProgrammers.filter(creditsList);

        System.out.println("\n" + "Выберите кредит");
        Inputs.getTypeCredit();// пользователь вводит число , которое равно id кредита, тем самым выбирая нужный ему кредит

        currentPercent = creditsList.get(typeCredit - 1).percent;         // Получаю переменную currentPercent которая равна процентной ставке
                                                                         // выбранного пользователем кредита

        System.out.println("Ставка по данному кредиту равна " + currentPercent + "%");
        Inputs.getChangeCredit(); // Здесь пользователь выбирает схему расчета (Аннуитентную или Дифферентную) выбранного кредита
        ChangeCredit.changeCredit();

    }
}